# Python Week Of Code, Namibia 2019

[mysite/urls.py](mysite/urls.py) has the list of Uniform Resource Locator (URL) supported by our website.

[blog/urls.py](blog/urls.py) has the list of URL supported by the blog on our website.

Every HTTP URL conforms to the following syntax

```
scheme://host[:port]path[?query][#fragment]
```

`scheme` is the protocol used for the communication, usually HTTPS.

`host` is the domain or address of the server that we want to connect.

`port` is the port on the server that we want to use. If not provide, we will use the default port defined by the `scheme`.

`path`is the location of the information that we are looking. Segments are separated by a slash (`/`).

`query` is a sequence of attribute–value pairs separated by a delimiter. Not used in Django very often.

`fragment` tells the browser where to scroll right after finish load the page.

## Task for Instructor

1. Run

   ```
   python manage.py migrate
   ```
2. Launch the Django server

   ```
   python manage.py runserver
   ```
3. Open http://localhost:8000/ in the web browser.
4. Open http://localhost:8000/?title=Welcome in the web browser.
5. Open http://localhost:8000/#main in the web browser.
6. Open http://localhost:8000/blog/welcome/ in the web browser.
7. Add

   ```
   path('blog/<title>/', views.blog, name='blog')
   ```

   to [blog/urls.py](blog/urls.py).
   You will notice the following error

   ```
     File "2019-08-24-controller-view/blog/urls.py", line 7, in <module>
       path('blog/<title>/', views.blog, name='blog')
   AttributeError: module 'blog.views' has no attribute 'blog'
   ```

## Tasks for Learners

None.
